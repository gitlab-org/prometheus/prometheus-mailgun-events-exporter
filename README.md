# Prometheus-Mailgun-Events-Exporter

Mailgun Events Prometheus exporter

## Description

This application is a prometheus exporter for mailgun Events, which aims to monitor the following things:

* `email_delivery_time_seconds`
* `email_delivery_error_messages`

## Configuration

The exporter expects a number of environment variables to run:

1. `MG_API_KEY` - The Mailgun API key
2. `MG_DOMAIN` - The Mailgun domain to monitor

`LOG_LEVEL` can be optionally configured with `debug`, `info`, or `error`.

## Authentication

Authentication towards the Mailgun API is being done with exp two ways:
To authenticate with Mailgun API, you need to set `MG_API_KEY`

## List of available metrics

```md
# HELP mailgun_delivery_time_seconds The time took for an email to actually got delivered from the time that got accepted in mailgun
# HELP mailgun_delivery_errors_total Email Delivery errors
# HELP mailgun_queued_accepted_events Number of accepted events waiting for matching delivered event
# HELP mailgun_expired_accepted_events_count Number of accepted events that have expired
```

## Release

The repository has automated builds configured in the DockerHub, for `main` branch and `latest` docker tag.

## How to pull the exporter

```sh
docker pull registry.gitlab.com/gitlab-org/prometheus/prometheus-mailgun-events-exporter:latest
```
