package config

import (
	"fmt"
	"os"
	"strings"
)

const (
	defaultLogLevel = "info"
	defaultDomain   = ""
	defaultApiKey   = ""
)

// Config contains all the available configuration options
type Config struct {
	Domain   string
	ApiKey   string
	LogLevel string
}

// FrontEnv populates the application configuration from environmental variables
func FromEnv() *Config {
	var (
		logLevel = getenv("LOG_LEVEL", defaultLogLevel)
		domain   = getenv("MG_DOMAIN", defaultDomain)
		apiKey   = getenv("MG_API_KEY", defaultApiKey)
	)

	c := &Config{
		LogLevel: strings.ToLower(logLevel),
		Domain:   domain,
		ApiKey:   apiKey,
	}
	return c
}

func getenv(key, fallback string) string {
	v := os.Getenv(key)
	if v == "" {
		return fallback
	}
	return v
}

func (config *Config) Validate() error {
	if config.ApiKey == "" {
		return fmt.Errorf("private API key not set via MG_API_KEY")
	}

	if config.Domain == "" {
		return fmt.Errorf("Mailgun domain not set via MG_DOMAIN")
	}

	return nil
}
